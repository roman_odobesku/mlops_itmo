import os

import click
import mlflow
import pandas as pd
from dotenv import load_dotenv
from etna.analysis import plot_forecast
from etna.metrics import SMAPE
from etna.models import CatBoostMultiSegmentModel
from etna.pipeline import Pipeline
from etna.transforms import (
    DateFlagsTransform,
    DensityOutliersTransform,
    FourierTransform,
    HolidayTransform,
    LagTransform,
    LinearTrendTransform,
    MeanTransform,
    SegmentEncoderTransform,
    TimeSeriesImputerTransform,
    TrendTransform,
)
from matplotlib import pyplot as plt

from src.data.train_test_split import train_test_split
from src.data.tsdataset import convert_to_tsdataset
from src.utils.logger import LOGGER

load_dotenv()


MLFLOW_TRACKING_URI = os.getenv("MLFLOW_TRACKING_URI")

mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)
mlflow.set_experiment("ETNA-CatBoost")


class MLFlowETNAPipeline(mlflow.pyfunc.PythonModel):
    def __init__(self, etna_pipeline: Pipeline):
        super().__init__()
        self.etna_pipeline = etna_pipeline

    def predict(self, context, data):
        model_output = self.etna_pipeline.forecast(ts=data)
        return model_output


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("horizon", type=click.INT, default=31)
@click.argument("pipeline_save_dir", type=click.Path())
@click.argument("forecast_plot_path", type=click.Path())
def train_cli(
    input_df_path: str,
    horizon: int,
    pipeline_save_dir: str,
    forecast_plot_path: str,
):
    """
    Производит обучение модели.
    Сохраняет пайплайн. Выводит значение метрики SMAPE.
    :param input_df_path: путь датафрейма на входе
    :param horizon: horizon
    :param pipeline_save_dir: куда сохранить пайплайн
    :param forecast_plot_path: куда сохранить пайплайн
    """
    df = pd.read_csv(input_df_path)
    LOGGER.info("Start")
    smape, forecast_ts = train(
        df, horizon, pipeline_save_dir, forecast_plot_path
    )

    LOGGER.info(f"SMAPE: {smape}")


def train(df, horizon, pipeline_save_dir, forecast_plot_path):
    with mlflow.start_run(run_name="ETNA-CatBoost"):
        mlflow.set_tag("Model", "CatBoostMultiSegmentModel")
        ts = convert_to_tsdataset(df)
        train_ts, test_ts = train_test_split(ts, horizon)

        transforms = [
            TimeSeriesImputerTransform(
                in_column="target", strategy="forward_fill"
            ),
            DensityOutliersTransform(in_column="target", distance_coef=3.0),
            LinearTrendTransform(in_column="target"),
            TrendTransform(in_column="target", out_column="trend"),
            LagTransform(
                in_column="target",
                lags=list(range(horizon, horizon * 3)),
                out_column="target_lag",
            ),
            DateFlagsTransform(
                day_number_in_week=True,
                day_number_in_month=True,
                week_number_in_month=True,
                month_number_in_year=True,
                season_number=True,
                is_weekend=True,
                out_column="date_flag",
            ),
            HolidayTransform(
                iso_code="RUS", mode="category", out_column="holiday_flag"
            ),  # in_column = 'timestamp',
            FourierTransform(period=360.25, order=6, out_column="fourier"),
            SegmentEncoderTransform(),
            MeanTransform(
                in_column=f"target_lag_{horizon}", window=12, seasonality=7
            ),
            MeanTransform(in_column=f"target_lag_{horizon}", window=7),
        ]

        model = CatBoostMultiSegmentModel()

        pipeline = Pipeline(
            model=model, transforms=transforms, horizon=horizon
        )

        LOGGER.info("Pipeline fit started")

        pipeline.fit(train_ts)

        LOGGER.info("Pipeline fit done")

        pipeline.save(pipeline_save_dir)

        LOGGER.info("Pipeline save done")

        forecast_ts = pipeline.forecast()

        LOGGER.info("Pipeline forecast done")

        plot_forecast(
            forecast_ts=forecast_ts,
            test_ts=test_ts,
            train_ts=train_ts,
            n_train_samples=90,
        )
        plt.savefig(forecast_plot_path, bbox_inches="tight")

        metric = SMAPE()  # считается по сегментам
        metric_value = metric(
            y_true=test_ts, y_pred=forecast_ts
        )  # example: {'mean_price': 11.492045838249387}

        mlflow.log_metric("SMAPE", metric_value["mean_price"])

        model_path = "ETNA-CatBoost"

        model = MLFlowETNAPipeline(pipeline)

        mlflow.pyfunc.log_model(
            artifact_path=model_path,
            python_model=model,
            registered_model_name="ETNA-CatBoost",
        )

        return metric_value, forecast_ts


if __name__ == "__main__":
    train_cli()
