import mlflow
from dotenv import load_dotenv

from src.utils.logger import LOGGER

load_dotenv()

if __name__ == "__main__":
    model_uri = "models:/ETNA-CatBoost/latest"
    loaded_model = mlflow.pyfunc.load_model(model_uri)
    preds = loaded_model.predict(None)

    LOGGER.info(preds)
