import click
import pandas as pd


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
def create_target_cli(input_df_path: str, output_df_path: str):
    """
    Формирует столбец target как среднее столбцов low и high.
    :param input_df_path: путь датафрейма на входе
    :param output_df_path: куда сохранить результирующий датафрейм
    """
    df = pd.read_csv(input_df_path)
    df = create_target(df)
    df.to_csv(output_df_path, index=False)


def create_target(df: pd.DataFrame):
    df_copy = df.copy()
    df_copy["target"] = (df_copy["low"] + df_copy["high"]) / 2
    return df_copy


if __name__ == "__main__":
    create_target_cli()
