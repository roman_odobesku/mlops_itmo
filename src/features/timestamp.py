import click
import pandas as pd


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
def create_timestamp_cli(input_df_path: str, output_df_path: str):
    """
    Считывает dataframe, преобразует столбец begin в timestamp
        и сохраняет результирующий dataframe.
    :param input_df_path: путь датафрейма на входе
    :param output_df_path: куда сохранить результирующий датафрейм
    """
    df = pd.read_csv(input_df_path)
    df = create_timestamp(df)
    df.to_csv(output_df_path, index=False)


def create_timestamp(df: pd.DataFrame, column="begin"):
    df_copy = df.copy()
    df_copy["timestamp"] = pd.to_datetime(df_copy[column])
    return df_copy


if __name__ == "__main__":
    create_timestamp_cli()
