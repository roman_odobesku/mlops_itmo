import click
import pandas as pd


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
def create_segment_cli(input_df_path: str, output_df_path: str):
    """
    Определяет сегменты в датафрейме.
    :param input_df_path: путь датафрейма на входе
    :param output_df_path: куда сохранить результирующий датафрейм
    """
    df = pd.read_csv(input_df_path)
    df = create_segment(df)
    df.to_csv(output_df_path, index=False)


def create_segment(df: pd.DataFrame):
    df_copy = df.copy()
    df_copy["segment"] = "mean_price"
    return df_copy


if __name__ == "__main__":
    create_segment_cli()
