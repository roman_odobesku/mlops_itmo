import click
import pandas as pd


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
def fill_missing_days_cli(input_df_path: str, output_df_path: str):
    """
    Заполняет пропущенные дни в датафрейме.
    target и segment заполняются через ffill,
    value и volume заполняются нулями.
    Остальные столбцы не заполняются.
    :param input_df_path: путь датафрейма на входе
    :param output_df_path: куда сохранить результирующий датафрейм
    """
    df = pd.read_csv(input_df_path)
    df = fill_missing_days(df)
    df.to_csv(output_df_path, index=False)


def fill_missing_days(df: pd.DataFrame):
    df["timestamp"] = pd.to_datetime(df["timestamp"])
    full_range = pd.date_range(
        start=df["timestamp"].min(), end=df["timestamp"].max(), freq="D"
    )
    full_df = pd.DataFrame(full_range, columns=["timestamp"])
    df_merged = pd.merge(full_df, df, on="timestamp", how="left")

    df_merged["target"] = df_merged["target"].ffill()
    df_merged["segment"] = df_merged["segment"].ffill()

    df_merged["value"] = df_merged["value"].fillna(0)
    df_merged["volume"] = df_merged["volume"].fillna(0)
    return df_merged


if __name__ == "__main__":
    fill_missing_days_cli()
