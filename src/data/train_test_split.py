from etna.datasets import TSDataset


def train_test_split(ts: TSDataset, horizon=31):
    train_ts, test_ts = ts.train_test_split(test_size=horizon)

    return train_ts, test_ts
