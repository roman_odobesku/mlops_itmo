import datetime
import os

import click
import pandas as pd
from dotenv import load_dotenv
from moexalgo import Ticker, session

load_dotenv()

username = os.getenv("MOEXALGO_USER")
password = os.getenv("MOEXALGO_PASS")
session.authorize(username, password)


@click.command()
@click.argument("ticker_name", type=click.STRING)
@click.argument("start", type=click.STRING)
@click.argument("period", type=click.STRING)
@click.argument("output_df_path", type=click.Path())
def get_candles_cli(
    ticker_name: str, start: str, period: str, output_df_path: str
):
    """
    Повзволяет получить свечи за указанный период и сохранить в csv файл.
    :param ticker_name: имя тикера, например SBER
    :param start: начальная дата получения данных, например 2011-01-01
    :param period: продолжительность свечек, например 1d
    :param output_df_path: куда сохранить csv
    """
    df = get_candles(
        ticker_name=ticker_name,  # "SBER",
        start=start,  # "2020-01-01",
        period=period,  # "1d",
        timeframe=datetime.timedelta(days=1),
    )
    df.to_csv(output_df_path, index=False)


def get_candles(
    ticker_name="SBER",
    start="2020-01-01",
    period="1d",
    timeframe=datetime.timedelta(days=1),
):
    sber = Ticker(ticker_name)

    all_candles = []

    while True:
        candles = pd.DataFrame(
            sber.candles(
                start=start, end=datetime.date.today(), period=period
            ),
            columns=[
                "begin",
                "end",
                "open",
                "high",
                "low",
                "close",
                "value",
                "volume",
            ],
        )

        all_candles.append(candles)

        candles["begin"] = pd.to_datetime(candles["begin"])
        candles["begin"] = candles["begin"].dt.strftime("%Y-%m-%d %H:%M:%S")

        date = pd.to_datetime(candles.iloc[-1]["begin"])
        start = date + timeframe

        if candles.shape[0] < 10000:
            break

    return pd.concat(all_candles)


if __name__ == "__main__":
    get_candles_cli()
