import pandas as pd
from etna.datasets import TSDataset


def convert_to_tsdataset(df=pd.DataFrame, freq="D"):
    ts = TSDataset(df, freq=freq)
    return ts
