import click
import pandas as pd


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
def drop_features_cli(input_df_path: str, output_df_path: str):
    """
    Удаляет столбцы "begin", "end", "open", "close", "low", "high".
    :param input_df_path: путь датафрейма на входе
    :param output_df_path: куда сохранить результирующий датафрейм
    """
    df = pd.read_csv(input_df_path)
    df = drop_features(df)
    df.to_csv(output_df_path, index=False)


def drop_features(df: pd.DataFrame):
    df_copy = df.copy()
    df_copy.drop(
        ["begin", "end", "open", "close", "low", "high"], axis=1, inplace=True
    )
    return df_copy


if __name__ == "__main__":
    drop_features_cli()
